import os

from py2neo import Graph, Node


class CreateNode:
    def __init__(self):
        self.g = Graph(
            host="192.168.1.221",  # neo4j 搭载服务器的ip地址，ifconfig可获取到
            http_port=7474,  # neo4j 服务器监听的端口号
            user="neo4j",  # 数据库user name，如果没有更改过，应该是neo4j
            password="ht###123")
        self.question_word_node()

    def question_word_node(self):
        self.self_type = ['什么是', '是什么']
        self.symptom_qwds = ['症状', '表征', '现象', '症候', '表现']
        self.cause_qwds = ['原因', '成因', '为什么', '怎么会', '怎样才', '咋样才', '怎样会', '如何会', '为啥', '为何', '如何才会', '怎么才会', '会导致',
                           '会造成']
        self.acompany_qwds = ['并发症', '并发', '一起发生', '一并发生', '一起出现', '一并出现', '一同发生', '一同出现', '伴随发生', '伴随', '共现']
        self.food_qwds = ['饮食', '饮用', '吃', '食', '伙食', '膳食', '喝', '菜', '忌口', '补品', '保健品', '食谱', '菜谱', '食用', '食物', '补品']
        self.drug_qwds = ['药', '药品', '用药', '胶囊', '口服液', '炎片']
        self.prevent_qwds = ['预防', '防范', '抵制', '抵御', '防止', '躲避', '逃避', '避开', '免得', '逃开', '避开', '避掉', '躲开', '躲掉', '绕开',
                             '怎样才能不', '怎么才能不', '咋样才能不', '咋才能不', '如何才能不',
                             '怎样才不', '怎么才不', '咋样才不', '咋才不', '如何才不',
                             '怎样才可以不', '怎么才可以不', '咋样才可以不', '咋才可以不', '如何可以不',
                             '怎样才可不', '怎么才可不', '咋样才可不', '咋才可不', '如何可不']
        self.lasttime_qwds = ['周期', '多久', '多长时间', '多少时间', '几天', '几年', '多少天', '多少小时', '几个小时', '多少年']
        self.cureway_qwds = ['怎么治疗', '如何医治', '怎么医治', '怎么治', '怎么医', '如何治', '医治方式', '疗法', '咋治', '怎么办', '咋办', '咋治']
        self.cureprob_qwds = ['多大概率能治好', '多大几率能治好', '治好希望大么', '几率', '几成', '比例', '可能性', '能治', '可治', '可以治', '可以医']
        self.easyget_qwds = ['易感人群', '容易感染', '易发人群', '什么人', '哪些人', '感染', '染上', '得上']
        self.check_qwds = ['检查', '检查项目', '查出', '检查', '测出', '试出']
        self.belong_qwds = ['属于什么科', '属于', '什么科', '科室']
        self.cure_qwds = ['治疗什么', '治啥', '治疗啥', '医治啥', '治愈啥', '主治啥', '主治什么', '有什么用', '有何用', '用处', '用途',
                          '有什么好处', '有什么益处', '有何益处', '用来', '用来做啥', '用来作甚', '需要', '要']
        cur_dir = '/'.join(os.path.abspath(__file__).split('/')[:-1])
        self.deny_path = os.path.join(cur_dir, 'dict/deny.txt')
        self.deny_words = [i.strip() for i in open(self.deny_path, encoding='utf_8') if i.strip()]
        # 构造领域actree

        self.words = [
            {
                'type': 'disease_desc',
                'words': self.self_type
            }, {
                'type': 'disease_symptom',
                'words': self.symptom_qwds
            }, {
                'type': 'disease_cause',
                'words': self.cause_qwds
            }, {
                'type': 'disease_acompany',
                'words': self.acompany_qwds
            }, {
                'type': 'disease_food',
                'words': self.food_qwds
            }, {
                'type': 'disease_drug',
                'words': self.drug_qwds
            }, {
                'type': 'disease_prevent',
                'words': self.prevent_qwds
            }, {
                'type': 'disease_lasttime',
                'words': self.lasttime_qwds
            }, {
                'type': 'disease_cureway',
                'words': self.cureway_qwds
            }, {
                'type': 'disease_cureprob',
                'words': self.cureprob_qwds
            }, {
                'type': 'disease_easyget',
                'words': self.easyget_qwds
            }, {
                'type': 'disease_check',
                'words': self.check_qwds
            }, {
                'type': 'belong_disease',
                'words': self.belong_qwds
            }, {
                'type': 'drug_disease',
                'words': self.cure_qwds
            }, {
                'type': 'food_deny',
                'words': self.deny_words
            }
        ]

    def find_all_cause(self):
        sql = 'match (n:cause) return n.name'
        ress = [i['n.name'] for i in self.g.run(sql).data()]
        print(ress)

    # 创建节点
    def create_node(self, label='question_type', words=None, type=''):
        if words is None:
            words = []
        count = 0
        for node_name in words:
            node = Node(label, name=node_name, label=type)
            self.g.create(node)
            count += 1
            print(count, len(words))
        return

    def handle(self):
        for word in self.words:
            self.create_node(words=word['words'],type=word['type'])
if __name__ == '__main__':
    cn = CreateNode()
    cn.handle()
