from py2neo import Graph,Node

class test:
    def __init__(self):
        self.g = Graph(host="127.0.0.1",  # neo4j 搭载服务器的ip地址，ifconfig可获取到
            http_port=7474,  # neo4j 服务器监听的端口号
            user="neo4j",  # 数据库user name，如果没有更改过，应该是neo4j
            password="806393858")

    def findNER(self,input):
        sql_name = 'match (n) where not n:question_type and  "{}" contains n.name return n.name as name,n.label as label'.format(input)
        sql_label = 'match (m:question_type) where "{}" contains m.name return m.label as label'.format(input)
        data_name = self.g.run(sql_name).data()
        data_label = self.g.run(sql_label).data()
        dict_type = {i['name']:i['label'] for i in data_name}
        stop_wds = []
        for wd1 in dict_type.keys():
            for wd2 in dict_type.keys():
                if wd1 in wd2 and wd1 != wd2:
                    stop_wds.append(wd1)

        final_wds = [i for i in dict_type.keys() if i not in stop_wds]
        final_dict = {i: dict_type.get(i) for i in final_wds}
        return final_dict,data_label


if __name__ == '__main__':
    test = test()
    while 1:
        text = input()
        print(test.findNER(text))